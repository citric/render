#[allow(unused_imports)]
use legion::prelude::*;

pub mod application;

/* TODO: Use as reference. Delete later.
use window::{
    EventLoop,
    WinitWindow,
};

pub async fn init() -> (EventLoop<()>, LocalState, Render, WinitWindow) {
    let universe = Universe::new();
    let mut world = universe.create_world();
    let (event_loop, window) = Window::new("rmbl")
        .expect("Could not initialize Window");
    let render = match Render::new(&window).await {
        Ok(state) => state,
        Err(e) => panic!(e),
    };
    let mut camera = render::Camera::default();
    camera.aspect = render.swapchain_descriptor.width as f32 / render.swapchain_descriptor.height as f32;

    let local_state = LocalState {
        mouse_x: 0.0,
        mouse_y: 0.0,
    };
    (event_loop, local_state, render, window)
}
*/

