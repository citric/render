#version 450

// input
layout(location = 0) in vec4 a_Pos;
layout(location = 1) in vec3 a_Norm;
layout(location = 2) in vec2 a_TexCoord;

// output
layout(location = 0) out vec4 v_Pos;
layout(location = 1) out vec3 v_Norm;
layout(location = 2) out vec2 v_TexCoord;

layout(set = 0, binding = 0) uniform Locals {
    mat4 u_Transform;
};

void main() {
    v_TexCoord = a_TexCoord;
    v_Norm = a_Norm;
    v_Pos = a_Pos;
    gl_Position = u_Transform * a_Pos;
}
