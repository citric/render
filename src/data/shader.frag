#version 450
// input
layout(location = 0) in vec4 v_Pos;
layout(location = 1) in vec3 v_Norm;
layout(location = 2) in vec2 v_TexCoord;

// output
layout(location = 0) out vec4 o_FragColor;

// uniforms
layout(set = 0, binding = 1) uniform texture2D t_Color;
layout(set = 0, binding = 2) uniform sampler s_Color;
//layout(set = 0, binding = 3) uniform Camera {
    //vec3 u_Camera;
//};

void main() {
    o_FragColor = texture(sampler2D(t_Color, s_Color), v_TexCoord);
}

