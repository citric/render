#version 450
#define MAX_STEPS 128
#define MAX_DIST 100.
#define SURF_DIST .001
#define MAX_REFLECT 3

// input
layout(location = 0) in vec4 v_Pos;
layout(location = 1) in vec3 v_Norm;
layout(location = 2) in vec2 v_TexCoord;

// output
layout(location = 0) out vec4 o_FragColor;

// uniforms
layout(set = 0, binding = 1) uniform texture2D t_Color;
layout(set = 0, binding = 2) uniform sampler s_Color;
//layout(set = 0, binding = 3) uniform Camera {
    //vec3 u_Camera;
//};

struct Ray {vec3 o; vec3 d;};
vec3 color(vec3 p);
float dist(vec3 p);
float light(vec3 p, vec3 lightPos);
float ray_march(Ray r);
vec3 reflect_col(Ray r);

vec3 color(Ray r) {
    return texture(sampler2D(t_Color, s_Color), v_TexCoord).xyz;
}

float light(vec3 p, vec3 lightPos) {
    vec3 l = normalize(lightPos-p);
    
    // check the angle between the light and the norm relative to the camera
    float dif = abs(dot(v_Norm, l));
    // march ray from a little in front of the vertex (so we don't accidentally collide
    // with the vertex itself) in the direction of the light
    //float d = ray_march(Ray(p + v_Norm * SURF_DIST, l));
    //if (length(lightPos - p) - d < SURF_DIST) dif = 0;

    return dif;
    //return 1.;
}

float ray_march(Ray r){
    float d0 = 0.;
    
    for(int i = 0; i < MAX_STEPS; i++){
        // Step ray
        vec3 p = vec3(r.o + r.d * d0);
        float dS = dist(p);
        d0 += dS;
        if(d0>MAX_DIST || dS<SURF_DIST) break;
    }
    return d0;
}

float dist(vec3 p){
    return length(p - v_Pos.xyz);
}

vec3 reflect_col(Ray r) {
    for (int i = 0; i < MAX_REFLECT; i++) {
        float d = ray_march(r);
        if (d == MAX_DIST) break;
        r.o = r.o + r.d * d;
        r.d = reflect(-r.d, v_Norm);
    }
    return color(r);
}

void main() {
    // vec4 tex = texture(sampler2D(t_Color, s_Color), v_TexCoord);
    // float mag = length(v_TexCoord-vec2(0.5));
    // o_FragColor = mix(tex, vec4(0.0), mag*mag);
    vec3 col    = vec3(0);
    vec3 ro     = vec3(0,1,0);
    vec3 rd     = vec3(0,0,1);
    Ray r       = Ray(ro, rd);
    float d     = ray_march(r);
    r.o = r.o + r.d * d;
    if (d >= SURF_DIST){
        col = color(r);
    }

    float l = light(r.o, vec3(0., 0., -10.));
    vec3 ref = reflect_col(r);
    if (ref != vec3(-1)) col = ref;
    col *= l;

    o_FragColor = vec4(col, 1.);
}

