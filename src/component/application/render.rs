use crate::state::pipeline::{
    Pipeline,
    Vertex,
    Entity,
};
use cgmath::{
    Matrix4,
    Point3,
    Vector3,
};
use legion::prelude::*;
use rayon::prelude::*;
use std::path::Path;
use tobj;
use wgpu::*;
use winit::window::Window;
use zerocopy::AsBytes;

pub const OPENGL_TO_WGPU_MATRIX: Matrix4<f32> = Matrix4::new(
    1.0, 0.0, 0.0, 0.0,
    0.0, -1.0, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.0, 0.0, 0.5, 1.0,
);

#[derive(Clone, Copy)]
pub struct Camera {
    pub eye: Point3<f32>,
    pub look: Vector3<f32>,
    pub up: Vector3<f32>,
    pub view: Matrix4<f32>,
    pub aspect: f32,
    pub fovy: f32,
    pub znear: f32,
    pub zfar: f32,
}

impl Default for Camera {
    fn default() -> Self {
        let eye = Point3::new(0f32, 0f32, 0f32);
        let look = -Vector3::unit_z();
        let up = Vector3::unit_y();
        let view = Matrix4::look_at_dir(
            eye,
            look,
            up,
        );
        let fovy = 100f32;
        let aspect = 16f32 / 9f32;
        let znear = 0.01;
        let zfar = 1000f32;
        let projection = cgmath::perspective(cgmath::Deg(fovy), aspect, znear, zfar);
        let correction = OPENGL_TO_WGPU_MATRIX;

        let view = correction * projection * view;
        Camera {
            eye,
            look,
            up,
            view,
            aspect,
            fovy,
            znear,
            zfar,
        }
    }
}

pub struct Render {
    pub device: Device,
    pub pipeline: Pipeline,
    pub queue: Queue,
    pub surface: Surface,
    pub swapchain: SwapChain,
    pub swapchain_descriptor: SwapChainDescriptor,
}

const DEPTH_FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Depth32Float;

impl Render {
    /// Creates a new, fully initialized Renderer.
    pub async fn new(window: &Window) -> Result<Self, &'static str> {
        let size = window.inner_size();
        // Creating Vulkan's basic necessities
        let surface = Surface::create(window);
        let adapter = Adapter::request(
            &RequestAdapterOptions {
                power_preference: PowerPreference::Default,
            },
            BackendBit::PRIMARY,
        ).await.expect("Could not retrieve adapter.");
        let (device, queue) = adapter.request_device(&DeviceDescriptor {
            extensions: Extensions {
                anisotropic_filtering: false,
            },
            limits: Limits::default(),
        }).await;

        let swapchain_descriptor = SwapChainDescriptor {
            usage: TextureUsage::OUTPUT_ATTACHMENT,
            format: TextureFormat::Bgra8UnormSrgb,
            width: size.width, 
            height: size.height,
            present_mode: PresentMode::Mailbox,
        };
        let swapchain = device.create_swap_chain(&surface, &swapchain_descriptor);

        Ok(Render {
            device,
            surface,
            pipeline: Pipeline::new().expect("Could not create empty pipeline"),
            swapchain,
            swapchain_descriptor,
            queue,
        })
    }

    pub async fn load_3d(render: &mut Render, mesh: &Path) -> Result<(), ()> {
        // TODO: Load gltf instead of obj
        let (models, _) = tobj::load_obj(mesh).expect("Could not load mesh");

        //models.par_iter_mut().map(|model| {
        for model in models {
            let mesh = &model.mesh;
            let mut vertex_data = Vec::new();
            let mut index_data = Vec::new();
            for i in &mesh.indices {
                index_data.push(*i as u16);
            }
            let mut positions = mesh.positions.iter();
            let mut normals = mesh.normals.iter();
            let mut texcoords = mesh.texcoords.iter();

            for _ in 0..mesh.positions.len() / 3 {
                let pos = [*positions.next().expect("Invalid position."),
                *positions.next().expect("Invalid position."),
                *positions.next().expect("Invalid position."),
                1.0];
                let norm = [*normals.next().expect("Invalid normal."),
                *normals.next().expect("Invalid normal."),
                *normals.next().expect("Invalid normal.")];
                let tex = [*texcoords.next().expect("Invalid texture coordinate."),
                *texcoords.next().expect("Invalid texture coordinate.")];
                let vertex = Vertex{pos, norm, tex,};
                //println!("{:?}", vertex);
                vertex_data.push(vertex);
            }

            //(vertex_data, index_data)
            &render.pipeline.entities.push(Entity {
                vertex_buf: render.device.create_buffer_with_data(vertex_data.as_bytes(), BufferUsage::VERTEX),
                index_buf: render.device.create_buffer_with_data(index_data.as_bytes(), BufferUsage::INDEX),
                index_count: index_data.len(),
                //bind_group: ,
                //uniform_buf: ,
            });
        }
        Ok(())
    }
}

pub fn render_system(render: &mut Render) {
    let frame = render.swapchain.get_next_texture()
        .expect("Timeout when acquiring swapchain texture.");
    let depth_texture = &render.device.create_texture(&TextureDescriptor{
        size: Extent3d {
            width: render.swapchain_descriptor.width,
            height: render.swapchain_descriptor.height,
            depth: 1,
        },
        mip_level_count: 1,
        sample_count: 1,
        dimension: TextureDimension::D2,
        format: DEPTH_FORMAT,
        usage: TextureUsage::OUTPUT_ATTACHMENT,
        array_layer_count: 1,
    });

    let forward_depth = depth_texture.create_default_view();
    let mut encoder = render.device
        .create_command_encoder(&CommandEncoderDescriptor { todo: 0 });
    {
        let mut rpass = encoder.begin_render_pass(&RenderPassDescriptor {
            color_attachments: &[RenderPassColorAttachmentDescriptor {
                attachment: &frame.view,
                resolve_target: None,
                load_op: LoadOp::Clear,
                store_op: StoreOp::Store,
                clear_color: Color {
                    r: 0.1,
                    g: 0.2,
                    b: 0.3,
                    a: 1.0,
                },
            }],
            depth_stencil_attachment: Some(RenderPassDepthStencilAttachmentDescriptor {
                attachment: &forward_depth,
                depth_load_op: LoadOp::Clear,
                depth_store_op: StoreOp::Store,
                stencil_load_op: LoadOp::Clear,
                stencil_store_op: StoreOp::Store,
                clear_depth: 1.0,
                clear_stencil: 0,
            }),
        });
        rpass.set_pipeline(&render.pipeline.pipeline.as_ref().expect("Pipeline doesn't exist"));
        rpass.set_bind_group(0, &render.pipeline.bind_group.as_ref().expect("Empty bind group"), &[]);
        for entity in &render.pipeline.entities {
            rpass.set_index_buffer(&entity.index_buf, 0, 0);
            rpass.set_vertex_buffer(0, &entity.vertex_buf, 0, 0);
            rpass.draw_indexed(0 .. entity.index_count as u32, 0, 0 .. 1);
        }
    }

    let command_buf = encoder.finish();
    render.queue.submit(&[command_buf]);
}

pub fn resize(render: &mut Render, camera: Camera) -> Option<wgpu::CommandBuffer> {
    let mx_total = camera.view_matrix();
    let mx_ref: &[f32; 16] = mx_total.as_ref();

    let temp_buf = render.device
        .create_buffer_with_data(mx_ref.as_bytes(), BufferUsage::COPY_SRC);

    let mut encoder = render.device
        .create_command_encoder(&CommandEncoderDescriptor { todo: 0 });
    encoder.copy_buffer_to_buffer(&temp_buf, 0, &render.pipeline.uniform_buf.as_ref().expect("Empty uniform buffer"), 0, 64);
    Some(encoder.finish())
}

