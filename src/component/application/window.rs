use std::time::Instant;
use winit::{
    event::{
        DeviceEvent,
        Event,
        VirtualKeyCode,
        WindowEvent,
    },
    event_loop::{
        ControlFlow,
    },
    window::{
        WindowBuilder,
    }
};
pub(crate) use winit::{
    event_loop::EventLoop,
    window::Window as WinitWindow,
};


#[derive(Debug)]
pub struct Window {
    event_loop: EventLoop<()>,
    title: String,
    window: WinitWindow,
}

impl Default for Window {
    fn default() -> Self {
        let event_loop = EventLoop::new();
        let title = "Title".to_string();
        let window = WindowBuilder::new()
            .with_title(title)
            .build(&event_loop)
            .expect("Could not create window.");
        Window {
            event_loop,
            title,
            window,
        }
    }
}

