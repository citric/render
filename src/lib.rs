use legion::world::{
    Universe,
    World,
};

mod component;
mod system;

use crate::component::application::{
    Render,
    Window,
};

pub fn init() -> (Universe, World) {
    let universe = Universe::new();
    let world = universe.create_world();

    let window = Window::default();
    let render = Render::new(window);
    let entities = world.insert(
        (),
        vec![
        (render, window),
        ],
    );
}

