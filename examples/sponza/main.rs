use futures::executor::block_on;
use std::path::Path;
mod state;

fn main() -> Result<(), ()> {
    simple_logger::init().expect("Could not initialize logger.");

    let (event_loop, _, mut render, window) = block_on(state::init());
    //block_on(render.load_mesh(&Path::new("assets/box.obj"))).expect("Could not load box");
    //block_on(render.load_mesh(&Path::new("assets/monkey.obj"))).expect("Could not load Suzanne");
    //block_on(render.load_mesh(&Path::new("assets/cube.obj"))).expect("Could not load cube");
    block_on(render.load_mesh(&Path::new("assets/sponza.obj"))).expect("Could not load Sponza");

    // TODO: Create main loop {} here. Push polled events to a vec. Move ControlFlow::Exit
    // to the end of every event_loop.run call.
    // This allows us to poll for events and then have the program run outside of the
    // events loop.
    state::WindowState::run(event_loop, render, window);

    Ok(())
}

